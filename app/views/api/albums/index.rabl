object false

node :albums do
  @albums.map do |album|
      {
        :id       => album["gphoto$id"]["$t"],
        :name     => album["gphoto$name"]["$t"],
        :thumb    => album["media$group"]["media$thumbnail"][0]["url"]
      }
  end
end

