class AlbumSerializer < ActiveModel::Serializer
  attributes :id, :name, :thumb
end
