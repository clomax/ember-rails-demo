class Api::AlbumsController < ApplicationController

  respond_to :json

  def index
    @user = current_user
    @albums = get_albums
  end


  private

  def get_albums
    if current_user
      response = JSON.parse(RestClient.get "https://picasaweb.google.com/data/feed/api/user/#{@user.email}?alt=json")
      response["feed"]["entry"]
    end
  end

end
