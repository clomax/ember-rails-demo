Rails.application.routes.draw do

  namespace :api do
    resources :albums, :images, only: :index
  end

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]
  resource  :home, only: [:show]

  root to: "home#index"
end
